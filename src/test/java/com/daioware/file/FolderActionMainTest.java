package com.daioware.file;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Map;
import java.util.function.Consumer;
import java.util.regex.Pattern;

import com.daioware.file.action.FolderAction;
import com.daioware.file.action.FolderAction.FileAction;
import com.daioware.language.compiler.parser.ParseException;
import com.daioware.language.compiler.parser.cmd.CommandArgumentsParser;
import com.daioware.file.action.FolderActionResult;

public class FolderActionMainTest {

	public static void main(String[] args) throws IOException, ParseException {
		CommandArgumentsParser commandParser=new CommandArgumentsParser();
		commandParser.setFlags(Arrays.asList("sc","fullPath","print","refresh"));
		Map<String,String> argsMap=commandParser.parse(args);
		StringBuilder info=new StringBuilder();
		Object object;
		Consumer<String> stringConsumer;
		boolean print=argsMap.containsKey("print");
		String resultFile=argsMap.get("res");
		if(print && resultFile!=null) {
			stringConsumer=(string)->{
				System.out.print(string);
				info.append(string);
			};
		}
		else if(resultFile!=null) {
			stringConsumer=(string)->{
				info.append(string);
			};
		}
		else if(print) {
			stringConsumer=(string)->{
				System.out.print(string);
			};
		}
		else {
			stringConsumer=(string)->{};
		}
		if(!validArguments(argsMap,stringConsumer)) {
			return;
		}
		File sourceFolder=new File(argsMap.get("0"));
		File destinationFolder=new File(argsMap.get("1"));
		Pattern patternForFilesToConsider=Pattern.compile(argsMap.getOrDefault("regex",".*"));
		FileAction fileAction=fromStringToFileAction(argsMap.get("a"));
		boolean saveChanges=argsMap.containsKey("sc");
		boolean useFullPath=argsMap.containsKey("fullPath");
		boolean deleteDestFilesThatAreNotInSrcFolder=argsMap.containsKey("refresh");

		String encoding=argsMap.getOrDefault("encoding","utf-8");
		object=argsMap.get("speed");
		int speed=object!=null?Integer.parseInt(object.toString()):FolderAction.DEFAULT_SPEED_IN_BYTES;
		
		FolderAction fileCopier=new FolderAction(sourceFolder, destinationFolder, patternForFilesToConsider,fileAction,
				speed,saveChanges,deleteDestFilesThatAreNotInSrcFolder);
		stringConsumer.accept(String.format("Source folder:%s\r\n", sourceFolder.getAbsolutePath()));
		stringConsumer.accept(String.format("Dest folder:%s\r\n", destinationFolder.getAbsolutePath()));
		stringConsumer.accept(String.format("Action:%s\r\n", fileAction.toString()));
		stringConsumer.accept(String.format("Regex:%s\r\n", patternForFilesToConsider.pattern()));
		
		if(!sourceFolder.isDirectory() || !sourceFolder.exists()) {
			stringConsumer.accept(String.format("%s doesn't exist or is not a directory\r\n", sourceFolder.getAbsolutePath()));
			return;
		}
		if(!destinationFolder.isDirectory() || !destinationFolder.exists()) {
			stringConsumer.accept(String.format("%s doesn't exist or is not a directory\r\n", destinationFolder.getAbsolutePath()));
			return;
		}
		FolderActionResult folderActionResult=fileCopier.copy();
		if(useFullPath) {
			stringConsumer.accept("*********Source Files updated***************\r\n");
			for(File file:folderActionResult.getSourceFilesUpdated()) {
				stringConsumer.accept(String.format("%s\r\n", file.getAbsolutePath()));
			}
			stringConsumer.accept("*********Destination Files updated**********\r\n");
			for(File file:folderActionResult.getDestinationFilesUpdated()) {
				stringConsumer.accept(String.format("%s\r\n", file.getAbsolutePath()));
			}
		}
		else {
			stringConsumer.accept("*********Source Files updated***************\r\n");
			for(File file:folderActionResult.getSourceFilesUpdated()) {
				stringConsumer.accept(String.format("%s\r\n", getPathFromParentFolder(sourceFolder,file)));
			}
			stringConsumer.accept("*********Destination Files updated**********\r\n");
			for(File file:folderActionResult.getDestinationFilesUpdated()) {
				stringConsumer.accept(String.format("%s\r\n", getPathFromParentFolder(destinationFolder,file)));
			}
		}
		stringConsumer.accept("Exceptions:\r\n");
		for(IOException ex:folderActionResult.getExceptions()) {
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			ex.printStackTrace(pw);
			stringConsumer.accept(String.format("%s\r\n", sw.toString()));
		}
		if(resultFile!=null) {
			Files.write(Paths.get(resultFile), info.toString().getBytes(encoding));		
		}
	}
	
	private static boolean validArguments(Map<String, String> argsMap,Consumer<String> stringConsumer) {
		boolean validArgs=true;
		if(!argsMap.containsKey("0")) {
			stringConsumer.accept("Source folder is missing\r\n");
			validArgs=false;
		}
		if(!argsMap.containsKey("1")) {
			stringConsumer.accept("Destination folder is missing\r\n");
			validArgs=false;
		}
		if(!argsMap.containsKey("a")) {
			stringConsumer.accept("Action is missing\r\n");
			validArgs=false;
		}
		return validArgs;
	}

	public static String getPathFromParentFolder(File parentFolder,File innerFile) {
		String fullPathInnerFile=innerFile.getAbsolutePath();
		return fullPathInnerFile.substring(parentFolder.getAbsolutePath().length()+1);
	}
	
	public static FileAction fromStringToFileAction(String fileAction) {
		switch(fileAction) {
			case "ma:":return FileAction.MOVE;
			case "mn":return FileAction.MOVE_IF_IT_IS_NEWER;
			case "md":return FileAction.MOVE_IF_IT_IS_DIFFERENT_DATE;
			case "od":return FileAction.OVERWRITE_IF_IT_IS_DIFFERENT_DATE;
			case "on":return FileAction.OVERWRITE_IF_IT_IS_NEWER;
			case "oa":default:return FileAction.OVERWRITE;
		}
	}
}
