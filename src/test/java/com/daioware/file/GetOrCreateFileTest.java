package com.daioware.file;

import static com.daioware.file.FileUtil.*;
import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.time.Instant;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import com.daioware.commons.string.StringUtil;

public class GetOrCreateFileTest {

	@Test
	public void test() throws IOException {
		String p=File.separator;
		String pre=StringUtil.getRandomString(10, StringUtil.DIGITS_AND_LETTERS)+Instant.now().getNano();
		List<String> folders=Arrays.asList(pre+"folderA",pre+"folderB",pre+"folderC",pre+"folderD");
		String path="";
		File aux;
		for(String folder:folders) {
			path+=folder+p;
		}
		path+="file.log";
		File file=getOrCreateFile(path);
		assertTrue(file.exists() && file.isFile());
		aux=file.getParentFile();
		if(!file.delete()) {
			throw new IOException("Could not delete "+file.getAbsolutePath());
		}
		while(aux!=null && folders.contains(aux.getName())) {
			if(!aux.delete()) {
				throw new IOException("Could not delete "+aux.getAbsolutePath());
			}
			aux=aux.getParentFile();
		}
	}

}
