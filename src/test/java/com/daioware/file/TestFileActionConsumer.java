package com.daioware.file;
import java.io.File;

import com.daioware.file.action.FileActionConsumer;

public class TestFileActionConsumer {

	public static void main(String[] args) {
		File srcFolder=new File("C:\\Users\\Diego Olvera\\Documents\\dev");
		File dstFolder=new File("X:\\Diego Olvera\\Documentos\\dev");
		FileActionConsumer consumer=new FileActionConsumer(srcFolder, dstFolder, 1024) {
			@Override
			public void accept(File t) {
				
			}
		};
		String filePath="X:\\Diego Olvera\\Documentos\\dev\\util\\backup\\test1.txt";
		File file=new File(filePath);
		System.out.println(file.exists());
		System.out.println(consumer.existsDestinationFileInSourceFolder(file));
	}
}
