package com.daioware.file.action;

import java.io.File;
import java.io.IOException;

import com.daioware.file.MyRandomAccessFile;

public class FileActionToCopy extends FileActionConsumer{
	
	
	public FileActionToCopy(File sourceFolder, File destinationFolder,int velocityInBytes,boolean saveChanges) {
		super(sourceFolder, destinationFolder,velocityInBytes,saveChanges);
	}

	@Override
	public void accept(File sourceFile) {
		int length=sourceFolder.getAbsolutePath().length();
		MyRandomAccessFile raSourceFile=null,raDestFile=null;
		File newFile=new File(destinationFolder.getAbsolutePath()+sourceFile.getAbsolutePath().substring(length));		
		try {
			createFoldersForNewFile(newFile);
			if(overwriteFile(sourceFile,newFile)) {
				addDestinationFileUpdated(newFile);
				raSourceFile=new MyRandomAccessFile(sourceFile, "r");
				raDestFile = new MyRandomAccessFile(newFile, "rw");
				raDestFile.setLength(0);
				raSourceFile.copy(raDestFile,raSourceFile.length(), getVelocityInBytes());
			}			
		} catch (IOException e) {
			exceptions.add(e);
			return;
		}
		finally {
			if(raSourceFile!=null) {
				try {
					raSourceFile.close();
				} catch (IOException e) {
					exceptions.add(e);
				}
			}
			if(raDestFile!=null) {
				try {
					raDestFile.close();
				} catch (IOException e) {
					exceptions.add(e);
				}
			}
		}
		
	}

	protected boolean overwriteFile(File sourceFile, File newFile) {
		return true;
	}

}
