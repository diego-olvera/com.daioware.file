package com.daioware.file.action;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class FolderActionResult {

	private List<IOException> exceptions;
	private List<File> sourceFilesUpdated;
	private List<File> destinationFilesUpdated;
	
	
	
	public FolderActionResult(List<IOException> exceptions, List<File> sourceFilesUpdated,
			List<File> destinationFilesUpdated) {
		super();
		setExceptions(exceptions);
		setSourceFilesUpdated(sourceFilesUpdated);
		setDestinationFilesUpdated(destinationFilesUpdated);
	}
	
	public List<IOException> getExceptions() {
		return exceptions;
	}
	
	public void setExceptions(List<IOException> exceptions) {
		this.exceptions = exceptions;
	}
	
	public List<File> getSourceFilesUpdated() {
		return sourceFilesUpdated;
	}
	
	public void setSourceFilesUpdated(List<File> sourceFilesUpdated) {
		this.sourceFilesUpdated = sourceFilesUpdated;
	}
	
	public List<File> getDestinationFilesUpdated() {
		return destinationFilesUpdated;
	}
	
	public void setDestinationFilesUpdated(List<File> destinationFilesUpdated) {
		this.destinationFilesUpdated = destinationFilesUpdated;
	}
	
	
}
