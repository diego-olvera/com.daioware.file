package com.daioware.file.action;

import java.io.File;

public class FileActionToCopyNewFile extends FileActionToCopy{

	public FileActionToCopyNewFile(File sourceFolder, File destinationFolder, int velocityInBytes,boolean saveChanges) {
		super(sourceFolder, destinationFolder, velocityInBytes,saveChanges);
	}

	@Override
	protected boolean overwriteFile(File sourceFile, File newFile) {
		return newFile.exists()?sourceFile.lastModified()>newFile.lastModified():true;
	}

	
}
