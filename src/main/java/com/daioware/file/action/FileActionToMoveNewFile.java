package com.daioware.file.action;

import java.io.File;
import java.io.IOException;

public class FileActionToMoveNewFile extends FileActionConsumer{
	
	
	public FileActionToMoveNewFile(File sourceFolder, File destinationFolder,int velocityInBytes,boolean saveChanges) {
		super(sourceFolder, destinationFolder,velocityInBytes,saveChanges);
	}

	@Override
	public void accept(File sourceFile) {
		int length=sourceFolder.getAbsolutePath().length();
		File newFile=new File(destinationFolder.getAbsolutePath()+sourceFile.getAbsolutePath().substring(length));
		try{
			createFoldersForNewFile(newFile);
		}catch(IOException exception) {
			exceptions.add(exception);
		}
		if(moveFile(sourceFile,newFile)) {
			if(sourceFile.renameTo(newFile)) {
				addSourceFileUpdated(sourceFile);
				addDestinationFileUpdated(newFile);
			}
			else {
				try{
					throw new IOException("Could not move '"+sourceFile.getAbsolutePath()+"' to '"+newFile.getAbsolutePath()+"'");
				}catch(IOException e) {
					exceptions.add(e);
				}
			}
		}			
	}

	protected boolean moveFile(File sourceFile, File newFile) {
		return newFile.exists()?sourceFile.lastModified()>newFile.lastModified():true;
	}

}
