package com.daioware.file.action;

import java.io.File;

public class FileActionToMoveDifferentFile extends FileActionToMove{
	
	public FileActionToMoveDifferentFile(File sourceFolder, File destinationFolder,int velocityInBytes,boolean saveChanges) {
		super(sourceFolder, destinationFolder,velocityInBytes,saveChanges);
	}

	protected boolean moveFile(File sourceFile, File newFile) {
		return newFile.exists()?sourceFile.lastModified()!=newFile.lastModified():true;
	}

}
