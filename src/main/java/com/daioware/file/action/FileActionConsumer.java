package com.daioware.file.action;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;
import java.util.function.Consumer;

public abstract class FileActionConsumer implements Consumer<File>{

	protected File sourceFolder;
	protected File destinationFolder;
	protected List<IOException> exceptions;
	protected int velocityInBytes;
	protected List<File> sourceFilesUpdated;
	protected List<File> destinationFilesUpdated;
	protected boolean saveChanges;

	public FileActionConsumer(File sourceFolder, File destinationFolder,int velocityInBytes) {
		this(sourceFolder,destinationFolder,velocityInBytes,false);
	}

	public FileActionConsumer(File sourceFolder, File destinationFolder,int velocityInBytes,boolean isSaveChanges) {
		super();
		setSourceFolder(sourceFolder);
		setDestinationFolder(destinationFolder);
		setVelocityInBytes(velocityInBytes);
		setSaveChanges(isSaveChanges);
	}

	public List<File> getSourceFilesUpdated() {
		return sourceFilesUpdated;
	}

	protected void setSourceFilesUpdated(List<File> sourceFilesUpdated) {
		this.sourceFilesUpdated = sourceFilesUpdated;
	}

	public List<File> getDestinationFilesUpdated() {
		return destinationFilesUpdated;
	}

	protected void setDestinationFilesUpdated(List<File> destinationFilesUpdated) {
		this.destinationFilesUpdated = destinationFilesUpdated;
	}

	public boolean isSaveChanges() {
		return saveChanges;
	}

	public void setSaveChanges(boolean saveChanges) {
		this.saveChanges = saveChanges;
	}

	public void initMetadata() {
		exceptions=new ArrayList<>();
		if(isSaveChanges()) {
			sourceFilesUpdated=new ArrayList<>();
			destinationFilesUpdated=new ArrayList<>();
		}
		else {
			sourceFilesUpdated=new EmptyList<>();
			destinationFilesUpdated=new EmptyList<>();
		}
	}
	
	public List<IOException> getExceptions() {
		return exceptions;
	}

	public File getSourceFolder() {
		return sourceFolder;
	}

	public void setSourceFolder(File sourceFolder) {
		this.sourceFolder = sourceFolder;
	}

	public File getDestinationFolder() {
		return destinationFolder;
	}

	public void setDestinationFolder(File destinationFolder) {
		this.destinationFolder = destinationFolder;
	}

	public int getVelocityInBytes() {
		return velocityInBytes;
	}

	public void setVelocityInBytes(int velocityInBytes) {
		this.velocityInBytes = velocityInBytes;
	}

	public void setExceptions(List<IOException> exceptions) {
		this.exceptions = exceptions;
	}
	
	public boolean addSourceFileUpdated(File file) {
		return sourceFilesUpdated.add(file);
	}
	
	public boolean addDestinationFileUpdated(File file) {
		return destinationFilesUpdated.add(file);
	}
	
	public boolean existsDestinationFileInSourceFolder(File destinationFile) {
		String destinationFileRelativePath=getRelativePathFromParentFolder(getDestinationFolder(),destinationFile);
		return new File(getSourceFolder()+File.separator+destinationFileRelativePath).exists();
	}
	
	public static void createFoldersForNewFile(File newFile) throws IOException {
		Stack<File> stackFolder=new Stack<>();
		if(!newFile.exists()) {
			File folder=newFile.getParentFile();
			while(folder!=null && !folder.exists()) {
				stackFolder.push(folder);
				folder=folder.getParentFile();
			}
			while(!stackFolder.isEmpty()) {
				if(!stackFolder.pop().mkdir()) {
					throw new IOException("Could not create folder "+folder.getAbsolutePath());
				}
			}
		}
	}
	
	public static String getRelativePathFromParentFolder(File parentFolder,File innerFile) {
		String fullPathInnerFile=innerFile.getAbsolutePath();
		return fullPathInnerFile.substring(parentFolder.getAbsolutePath().length()+1);
	}

}
