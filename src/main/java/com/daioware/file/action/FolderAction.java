package com.daioware.file.action;

import java.io.File;
import java.io.IOException;
import java.util.function.Consumer;
import java.util.regex.Pattern;

import com.daioware.file.FileFinder;

public class FolderAction {
	
	public static final int DEFAULT_SPEED_IN_BYTES=1024;
	
	public static enum FileAction{
		OVERWRITE,
		OVERWRITE_IF_IT_IS_NEWER,
		OVERWRITE_IF_IT_IS_DIFFERENT_DATE,
		
		MOVE_IF_IT_IS_NEWER,
		MOVE_IF_IT_IS_DIFFERENT_DATE,
		MOVE,
	}
	
	private File sourceFolder;
	private File destinationFolder;
	private Pattern patternForFilesToConsider;
	private FileAction fileAction;
	private FileActionConsumer fileActionConsumer;
	private int velocityInBytes;
	private boolean saveChanges;
	private boolean deleteDestinationFilesThatAreNotInSourceFolder;
	
	public FolderAction(File sourceFolder, File destinationFolder, Pattern patternForFilesToConsider,FileAction fileAction) {
		this(sourceFolder,destinationFolder,patternForFilesToConsider,fileAction,1024,false,false);
	}
	
	public FolderAction(File sourceFolder, File destinationFolder, Pattern patternForFilesToConsider,FileAction fileAction,
			int velocityInBytes,boolean saveChanges,boolean deleteDestinationFilesThatAreNotInSourceFolder) {
		super();
		setSourceFolder(sourceFolder);
		setDestinationFolder(destinationFolder);
		setPatternForFilesToConsider(patternForFilesToConsider);
		setFileAction(fileAction);
		setVelocityInBytes(velocityInBytes);
		setSaveChanges(saveChanges);
		setDeleteDestinationFilesThatAreNotInSourceFolder(deleteDestinationFilesThatAreNotInSourceFolder);
	}

	public boolean isDeleteDestinationFilesThatAreNotInSourceFolder() {
		return deleteDestinationFilesThatAreNotInSourceFolder;
	}

	public void setDeleteDestinationFilesThatAreNotInSourceFolder(boolean deleteDestinationFilesThatAreNotInSourceFolder) {
		this.deleteDestinationFilesThatAreNotInSourceFolder = deleteDestinationFilesThatAreNotInSourceFolder;
	}

	public boolean isSaveChanges() {
		return saveChanges;
	}

	public void setSaveChanges(boolean saveChanges) {
		this.saveChanges = saveChanges;
	}

	public int getVelocityInBytes() {
		return velocityInBytes;
	}

	public void setVelocityInBytes(int velocityInBytes) {
		this.velocityInBytes = velocityInBytes;
	}

	public FileAction getFileAction() {
		return fileAction;
	}


	protected FileActionConsumer getFileActionConsumer() {
		return fileActionConsumer;
	}

	protected void setFileActionConsumer(FileActionConsumer fileActionConsumer) {
		this.fileActionConsumer = fileActionConsumer;
	}

	public void setFileAction(FileAction fileAction) {
		this.fileAction = fileAction;
	}


	public Consumer<File> getConsumer() {
		return fileActionConsumer;
	}


	public void setConsumer(FileActionConsumer consumer) {
		this.fileActionConsumer = consumer;
	}


	public File getSourceFolder() {
		return sourceFolder;
	}


	public void setSourceFolder(File sourceFolder) {
		this.sourceFolder = sourceFolder;
	}


	public File getDestinationFolder() {
		return destinationFolder;
	}


	public void setDestinationFolder(File destinationFolder) {
		this.destinationFolder = destinationFolder;
	}


	public Pattern getPatternForFilesToConsider() {
		return patternForFilesToConsider;
	}


	public void setPatternForFilesToConsider(Pattern patternForFilesToConsider) {
		this.patternForFilesToConsider = patternForFilesToConsider;
	}
	
	protected void initializeFileActionConsumer() {
		switch(getFileAction()) {
			case MOVE_IF_IT_IS_NEWER:
				setFileActionConsumer(new FileActionToMoveNewFile(getSourceFolder(), getDestinationFolder(), 
						getVelocityInBytes(),isSaveChanges()));
				break;
			case MOVE:
				setFileActionConsumer(new FileActionToMove(getSourceFolder(), getDestinationFolder(),
						getVelocityInBytes(),isSaveChanges()));
				break;
			case MOVE_IF_IT_IS_DIFFERENT_DATE:
				setFileActionConsumer(new FileActionToMoveDifferentFile(getSourceFolder(), getDestinationFolder(),
						getVelocityInBytes(),isSaveChanges()));
				break;
			case OVERWRITE_IF_IT_IS_NEWER: 
				setFileActionConsumer(new FileActionToCopyNewFile(getSourceFolder(), getDestinationFolder(), 
						getVelocityInBytes(),isSaveChanges()));
				break;
			case OVERWRITE:default: 
				setFileActionConsumer(new FileActionToCopy(getSourceFolder(),getDestinationFolder(),
						getVelocityInBytes(),isSaveChanges()));
		}
	}
	
	public boolean isCopyBehavior() {
		switch(getFileAction()) {
			case OVERWRITE:case OVERWRITE_IF_IT_IS_DIFFERENT_DATE:case OVERWRITE_IF_IT_IS_NEWER:return true;
			default:return false;
		}
	}
	public FolderActionResult copy() {
		initializeFileActionConsumer();	
		FileActionConsumer fileActionConsumer=getFileActionConsumer();
		fileActionConsumer.initMetadata();
		FileFinder.findFiles(getSourceFolder(), getPatternForFilesToConsider(), new EmptyList<File>(), fileActionConsumer);
		if(isDeleteDestinationFilesThatAreNotInSourceFolder() && isCopyBehavior()) {
			FileFinder.getAllFiles(getDestinationFolder(), new EmptyList<File>(),
					file->{
						if(!fileActionConsumer.existsDestinationFileInSourceFolder(file)) {
							if(file.delete()) {
								fileActionConsumer.addDestinationFileUpdated(file);
							}
							else {
								try{
									throw new IOException("Could not delete file "+file.getAbsolutePath());
								}catch(IOException e) {
									fileActionConsumer.exceptions.add(e);
								}
							}
						}
					}
			);
		}
		return new FolderActionResult(fileActionConsumer.getExceptions(), fileActionConsumer.getSourceFilesUpdated(), 
				fileActionConsumer.getDestinationFilesUpdated());
	}
	
}
