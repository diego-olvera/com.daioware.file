package com.daioware.file.action;

import java.io.File;

public class FileActionToCopyDifferentFile extends FileActionToCopy{
	
	
	public FileActionToCopyDifferentFile(File sourceFolder, File destinationFolder,int velocityInBytes,boolean saveChanges) {
		super(sourceFolder, destinationFolder,velocityInBytes,saveChanges);
	}

	protected boolean overwriteFile(File sourceFile, File newFile) {
		return newFile.exists()?sourceFile.lastModified()!=newFile.lastModified():true;
	}

}
