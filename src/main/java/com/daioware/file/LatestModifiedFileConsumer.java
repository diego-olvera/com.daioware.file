package com.daioware.file;

import java.io.File;
import java.util.function.Consumer;

public class LatestModifiedFileConsumer implements Consumer<File>{
	
	private File latestModifiedFile;
	@Override
	public void accept(File file) {
		File latest=getLatestModifiedFile();
		if(latest==null) {
			setLatestModifiedFile(file);
		}
		else if(file.lastModified()>latest.lastModified()) {
			setLatestModifiedFile(file);
		}
		//else latestModifiedFile remains the same
		
	}
	public File getLatestModifiedFile() {
		return latestModifiedFile;
	}
	protected void setLatestModifiedFile(File latestModifiedFile) {
		this.latestModifiedFile = latestModifiedFile;
	}

}
