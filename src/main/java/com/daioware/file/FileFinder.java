package com.daioware.file;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Consumer;
import java.util.regex.Pattern;

public class FileFinder {
	
	private static final Consumer<File> emptyFileConsumer=(f)->{};
	
	public static List<File> getAllFiles(File path){
		return getAllFiles(path,emptyFileConsumer);
	}
	
	public static List<File> getAllFiles(File path,Consumer<File> fileConsumer){
		return getAllFiles(path,new ArrayList<>(),fileConsumer);
	}
	
	public static List<File> getAllFiles(File path,List<File> files,Consumer<File> fileConsumer){
	 	List<File> filesQueue=new LinkedList<>();
	 	filesQueue.add(path);
	 	do {
	 		for(File file:filesQueue.remove(0).listFiles()) {
				if(file.isDirectory()) {
					filesQueue.add(file);
				}
				else{
					files.add(file);
					fileConsumer.accept(file);
				}
			}
	 	}while(!filesQueue.isEmpty());
		return files;
	}
	
	public static List<File> findFiles(File path,Pattern pattern,List<File> files,Consumer<File> fileConsumer){
		//Recursive solution
		/*for(File file:path.listFiles()) {
			if(file.isDirectory()) {
				findFiles(file,pattern,files);
			}
			else if(pattern.matcher(file.getAbsolutePath()).matches()) {
				files.add(file);
				fileConsumer.accept(file);
			}
		}*/
		//Queue solution,
	 	List<File> filesQueue=new LinkedList<>();
	 	filesQueue.add(path);
	 	do {
	 		for(File file:filesQueue.remove(0).listFiles()) {
				if(file.isDirectory()) {
					filesQueue.add(file);
				}
				else if(pattern.matcher(file.getAbsolutePath()).matches()){
					files.add(file);
					fileConsumer.accept(file);
				}
			}
	 	}while(!filesQueue.isEmpty());
		return files;
	}
	
	public static List<File> findFiles(File path,Pattern pattern,List<File> files){
		return findFiles(path, pattern,files,emptyFileConsumer);
	}
	
	public static List<File> findFiles(File path,String regex){		
		return findFiles(path,regex,new ArrayList<File>());
	}
	
	public static List<File> findFiles(File path,String regex,List<File> files){	
		return findFiles(path,Pattern.compile(regex),new ArrayList<File>());
	}
	
	public static List<File> findFilesByExt(File path,String ext){		
		return findFilesByExt(path,ext,new ArrayList<File>());
	}
	
	public static List<File> findFilesByExt(File path,String ext,List<File> files){
		return findFiles(path,Pattern.compile(".*"+ext),files);
	}
	
	public static List<File> findFilesByExts(File path,List<String> exts,List<File> files){
		return findFiles(path,Pattern.compile(FilePattern.toPattern(exts)),files);
	}
	
	public static List<File> findFilesByExts(File path,List<String> exts){
		return findFilesByExts(path,exts,new ArrayList<File>());
	}		
	
	public static List<File> findFiles(File path,Pattern pattern){
		return findFiles(path,pattern,new ArrayList<>(0));
	}
}
