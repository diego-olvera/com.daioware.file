package com.daioware.file;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

import com.daioware.commons.string.StringUtil;
import com.daioware.file.compress.Zip;

public class VersionControl {
	private File file;
	private String descriptionOfNewVersion;
	public VersionControl(String fileName){
		this(new File(fileName));
	}
	public VersionControl(File file){
		this(file,null);
	}
	public VersionControl(File file,String descriptionOfNewVersion){
		setFile(file);
		setDescriptionOfNewVersion(descriptionOfNewVersion);
	}
	public VersionControl(String file,String descriptionOfNewVersion){
		this(new File(file),descriptionOfNewVersion);
	}
	public String getDescriptionOfNewVersion() {
		return descriptionOfNewVersion;
	}
	public boolean setDescriptionOfNewVersion(String i) {
		if(StringUtil.isValidSentence(i)){
			descriptionOfNewVersion=i;
			return true;
		}
		return false;
	}
	public String getCompleteFileName(){
		return getFile().getAbsolutePath();
	}
	public File getFile(){
		return file;
	}
	public boolean setFile(File a){
		if(a!=null){
			file=a;
			return true;
		}
		return false;
	}
	public boolean setFileName(String n){
		return setFile(new File(n));
	}
	public String getNewNameVersion(){
		return getCompleteFileName()+" "+getCurrentVersionName()+".zip";
	}
	public String getCurrentVersionName(){
		DateTimeFormatter form=DateTimeFormatter.ofPattern("YYYY-MM-dd h:m:s");
		return LocalDateTime.now(ZoneId.systemDefault()).format(form);
	}
	public boolean createNewVersion(){
		return createNewVersion(getNewNameVersion());
	}
	public boolean createNewVersion(String versionName){ 
		String infoNextVersion;
		MyRandomAccessFile archInfo;
		try {
			if(new File(versionName).createNewFile()){
				Zip.zip(getCompleteFileName(),versionName);
				if((infoNextVersion=getDescriptionOfNewVersion())!=null){
					archInfo=new MyRandomAccessFile(versionName+".txt","rw");
					archInfo.writeUTFCharacters(infoNextVersion);
					archInfo.close();
				}
				return true;
			} 
			else{
				return false;
			}		
		} 
		catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}
	public static boolean createNewVersion(File folder,String description) {
		return new VersionControl(folder, description).createNewVersion();
	}
	public static boolean createNewVersion(String folder,String description) {
		return createNewVersion(new File(folder),description);
	}
}
