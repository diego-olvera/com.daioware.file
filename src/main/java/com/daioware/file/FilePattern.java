package com.daioware.file;

import java.util.List;

public class FilePattern {

	public static String toPattern(List<String> extensions) {
		StringBuilder extsBuilder=new StringBuilder(".*(");
		String sep="";
		for(String ext:extensions) {
			extsBuilder.append(sep).append(ext);
			sep="|";
		}
		return extsBuilder.append(")$").toString();
	}
}
