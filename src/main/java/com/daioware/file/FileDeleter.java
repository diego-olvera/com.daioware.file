package com.daioware.file;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.regex.Pattern;

import static com.daioware.file.FileFinder.*;

public class FileDeleter {

	private static final Consumer<File> fileConsumer=(f)->{f.delete();};
	
	public static List<File> deleteAllFiles(File path){
		return deleteAllFiles(path,fileConsumer);
	}
	
	public static List<File> deleteAllFiles(File path,Consumer<File> fileConsumer){
		return deleteAllFiles(path,new ArrayList<>(),fileConsumer);
	}
	
	public static List<File> deleteAllFiles(File path,List<File> files,Consumer<File> fileConsumer){
		return getAllFiles(path, files,fileConsumer);
	}
	
	public List<File> deleteFiles(File path,Pattern pattern,List<File> files){
		return findFiles(path, pattern,files,fileConsumer);
	}
	
	public static List<File> deleteFiles(File path,String regex){		
		return findFiles(path,regex,new ArrayList<File>());
	}
	
	public static List<File> deleteFiles(File path,String regex,List<File> files){	
		return findFiles(path,Pattern.compile(regex),new ArrayList<File>());
	}
	
	public static List<File> deleteFilesByExt(File path,String ext){		
		return findFiles(path,ext,new ArrayList<File>());
	}
	
	public static List<File> deleteFilesByExt(File path,String ext,List<File> files){
		return findFiles(path,Pattern.compile(".*"+ext),files);
	}
	
	public static List<File> deleteFilesByExts(File path,List<String> exts,List<File> files){
		return findFiles(path,Pattern.compile(FilePattern.toPattern(exts)),files);
	}
	
	public static List<File> deleteFilesByExts(File path,List<String> exts){
		return findFilesByExts(path,exts,new ArrayList<File>());
	}		
	
	public static List<File> deleteFiles(File path,Pattern pattern){
		return findFiles(path,pattern,new ArrayList<>(0));
	}
	
}
