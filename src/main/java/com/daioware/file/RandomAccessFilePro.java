package com.daioware.file;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

public class RandomAccessFilePro {

	private File file;
	private MyRandomAccessFile raf;
	
	public RandomAccessFilePro(String file,String mode) throws FileNotFoundException {
		this(new File(file),mode);
	}
	
	public RandomAccessFilePro(File file, String mode) throws FileNotFoundException {
		setFile(file);
		open(mode);
	}

	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}

	public MyRandomAccessFile getMyRandomAccessFile() {
		return raf;
	}
	
	private void open(String mode) throws FileNotFoundException {
		raf=new MyRandomAccessFile(getFile(), mode);
	}
	
	public void open(File file,String mode) throws FileNotFoundException {
		setFile(file);
		open(mode);
	}
	
	public void close() throws IOException {
		raf.close();
	}
}
